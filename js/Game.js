import * as ROT from 'rot-js';

import Player from './Player';
import Enemy from './Enemy';
import {
  KEY_MAP, DIRECTIONS, MAX_NUM_BOXES, KEY_ENTER, KEY_SPACEBAR,
} from './config';

const PRIZE_INDEX = 0;

// make GameMap class
export default class Game {
  constructor() {
    this.display = null;
    this.engine = null;
    this.player = null;
    this.enemy = null;
    this.prize = null;
    this.map = {};
  }

  init() {
    this.display = new ROT.Display({ spacing: 1.1 });

    document.body.appendChild(this.display.getContainer());

    this._generateMap();

    const scheduler = new ROT.Scheduler.Simple();
    scheduler.add(this.player, true);
    scheduler.add(this.enemy, true);

    this.engine = new ROT.Engine(scheduler);
    this.engine.start();
  }

  _generateMap() {
    const digger = new ROT.Map.Digger();
    const freeCells = [];

    const digCallback = (x, y, value) => {
      // do not store walls
      if (value) {
        return;
      }

      const key = `${x},${y}`;
      freeCells.push(key);

      this.map[key] = '.';
    }

    digger.create(digCallback);
    // problem - the map itself and map items are here
    this._generateBoxes(freeCells);
    this._drawWholeMap();
    this._createPlayer(freeCells);
    this._createEnemy(freeCells);
  }

  _drawWholeMap() {
    for (const key in this.map) {
      const [x, y] = this._getKeyParts(key);

      this.display.draw(x, y, this.map[key]);
    }
  }

  _generateBoxes(freeCells) {
    for (let i = 0; i < MAX_NUM_BOXES; i += 1) {
      const key = this._getFreeCellKey(freeCells);

      this.map[key] = '*';

      if (i === PRIZE_INDEX) {
        this.prize = key;
      }
    }
  }

  _createPlayer(freeCells) {
    const key = this._getFreeCellKey(freeCells);
    const [x, y] = this._getKeyParts(key);

    this.player = new Player(x, y, () => {
      this.engine.lock();
      window.addEventListener('keydown', this);
    });

    this.display.draw(this.player._x, this.player._y, '@', '#ff0');
  }

  _createEnemy(freeCells) {
    const key = this._getFreeCellKey(freeCells);
    const [x, y] = this._getKeyParts(key);

    this.enemy = new Enemy(x, y, () => {
      const [playerX, playerY] = this.player.getCoords();

      const passableCallback = (coordX, coordY) => {
        const coords = `${coordX},${coordY}`;
        return (coords in this.map);
      }

      const astar = new ROT.Path.AStar(playerX, playerY, passableCallback, { topology: 4 });

      const path = [];
      const pathCallback = function (pathX, pathY) {
        path.push([pathX, pathY]);
      }
      astar.compute(this.enemy._x, this.enemy._y, pathCallback);

      path.shift();
      if (path.length === 1) {
        this.engine.lock();
        console.log('Game over - you were captured by Shadow!');
      } else {
        const [[newX, newY]] = path;

        const oldPositionKey = `${this.enemy._x},${this.enemy._y}`;

        this.display.draw(this.enemy._x, this.enemy._y, this.map[oldPositionKey]);
        this.enemy._x = newX;
        this.enemy._y = newY;
        this.display.draw(this.enemy._x, this.enemy._y, 'P', 'red');
      }
    });

    this.display.draw(this.enemy._x, this.enemy._y, 'P', 'red');
  }

  _getFreeCellKey(freeCells) {
    const index = Math.floor(ROT.RNG.getUniform() * freeCells.length);
    const [key] = freeCells.splice(index, 1);

    return key;
  }

  _getKeyParts(key) {
    const parts = key.split(',');
    const [x, y] = parts.map(part => parseInt(part));

    return [x, y];
  }

  handleEvent(e) {
    const code = e.keyCode;
    const key = `${this.player._x},${this.player._y}`;

    if (code === KEY_ENTER || code === KEY_SPACEBAR) {
      this._checkBox(key);
      return;
    }

    /* one of numpad directions? */
    // separate moving logic
    if (!(code in KEY_MAP)) {
      return;
    }

    /* is there a free space? */
    // move to Player method
    const [dirX, dirY] = ROT.DIRS[DIRECTIONS][KEY_MAP[code]];
    const newX = this.player._x + dirX;
    const newY = this.player._y + dirY;
    const newKey = `${newX},${newY}`;
    if (!(newKey in this.map)) {
      return;
    }

    this.display.draw(this.player._x, this.player._y, this.map[key]);
    this.player._x = newX;
    this.player._y = newY;
    this.display.draw(this.player._x, this.player._y, '@', '#ff0');
    window.removeEventListener('keydown', this);
    this.engine.unlock();
  }

  _checkBox(key) {
    if (this.map[key] !== '*') {
      console.log('There is no box here!');
    } else if (key === this.prize) {
      console.log('You found a light and won this game.');
      this.engine.lock();
      window.removeEventListener('keydown', this);
    } else {
      console.log('This box is empty.');
    }
  }
}
