import GameCharacter from './GameCharacter';

export default class Enemy extends GameCharacter {
  getDrawParams() {
    return [this._x, this._y, 'P', 'red'];
  }
}
