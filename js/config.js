const KEY_UP = 38;
const KEY_RIGHT_UP = 33;
const KEY_RIGHT = 39;
const KEY_RIGHT_DOWN = 34;
const KEY_DOWN = 40;
const KEY_LEFT_DOWN = 35;
const KEY_LEFT = 37;
const KEY_LEFT_UP = 36;
const KEY_ENTER = 13;
const KEY_SPACEBAR = 32;

const KEY_MAP = {
  [KEY_UP]: 0,
  [KEY_RIGHT_UP]: 1,
  [KEY_RIGHT]: 2,
  [KEY_RIGHT_DOWN]: 3,
  [KEY_DOWN]: 4,
  [KEY_LEFT_DOWN]: 5,
  [KEY_LEFT]: 6,
  [KEY_LEFT_UP]: 7,
};

const MAX_NUM_BOXES = 10;
const DIRECTIONS = 8;

export {
  KEY_MAP,
  MAX_NUM_BOXES,
  DIRECTIONS,
  KEY_ENTER,
  KEY_SPACEBAR,
}
