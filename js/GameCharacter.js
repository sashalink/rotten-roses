const DEFAULT_SPEED = 100;

export default class GameCharacter {
  constructor(x, y, onAct) {
    this._x = x;
    this._y = y;
    this._onAct = onAct;
  }

  act() {
    this._onAct();
  }

  getSpeed() {
    return DEFAULT_SPEED;
  }

  getCoords() {
    return [this._x, this._y];
  }
}
